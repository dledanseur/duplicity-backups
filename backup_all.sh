#/bin/bash

SCRIPT_PATH="`dirname \"$0\"`"
BACKUP_LOG="/var/log/duplicity-backups.log"
MAIL=`which mail`



failure=0

err_report() {
  failure=1
}

# Parse arguments if any
# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

# Initialize our own variables:
conf_dir=${SCRIPT_PATH}conf.d
script_name="ALL"

while getopts "d:b:" opt; do
    case "$opt" in
    d)  conf_dir=$OPTARG
        ;;
    b)  script_name=$OPTARG
        ;;
    esac
done

shift $((OPTIND-1))

[ "$1" = "--" ] && shift


# Remote any previous log
if [ -f $BACKUP_LOG ]; then
  rm $BACKUP_LOG
fi

echo "Executing backups with PATH=${PATH}" >> $BACKUP_LOG

# Launch a backup for each file in the conf subdir
for f in `ls -d -1 ${conf_dir}*.conf`
do
  echo "Handling file $f, basename is $(/usr/bin/basename $f), script name is $script_name" >> $BACKUP_LOG

  if [ $script_name != "ALL" ] && [ $script_name != $(/usr/bin/basename $f) ]; then
    continue
  fi

  unset PRE_CMD
  unset POST_CMD

  [ -e /etc/master-backup.conf ] && . /etc/master-backup.conf

  . $f

  echo "Running pre command: $PRE_CMD" >> $BACKUP_LOG
  $PRE_CMD >> $BACKUP_LOG
  
  if [ "$?" -eq 0 ]; then
   
    echo "Launching backup for $f" >> $BACKUP_LOG
    ${SCRIPT_PATH}/master-backup.sh --conf $f --no-email --report-status >> $BACKUP_LOG
    if [ $? -ne 0 ]; then
      echo "Backup for $f did not work" >> $BACKUP_LOG
      err_report
    fi
 
  else
    err_report 
  fi

  echo "Running post command: $POST_CMD" >> $BACKUP_LOG
  $POST_CMD >> $BACKUP_LOG

  if [ "$?" -ne 0 ]; then
    err_report
  fi

  echo "-------------------------------" >> $BACKUP_LOG

done

# Send mail
SUBJECT="All backups went fine"
if [ $failure -eq 1 ]; then
  SUBJECT="Some backups failed!"
fi


if [ "$EMAIL" != "" ]; then
cat $BACKUP_LOG|$MAIL -s "$SUBJECT" "$EMAIL"
fi



