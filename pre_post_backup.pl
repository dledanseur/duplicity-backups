#!/usr/bin/perl

use DBI;

# parse arguments
my @mysql=();
my @snapshots=();
my @ssh_services=();
my @ssh_commands=();

my $mysql_index=-1;
my $snapshot_index=-1;
my $ssh_services_index=-1;
my $ssh_commands_index=-1;

my $command = shift(@ARGV);

@ARR = @ARGV;


while ($element = shift(@ARR)) {
  if ($element eq "-f") {
    my $file = shift(@ARR);
    @newarr = ();
    open(my $fh, '<:encoding(UTF-8)', $file)  or die "Could not open file '$file' $!";
    while (my $row = <$fh>) {
      chomp $row;
      push(@newarr,$row);
    }
    unshift(@ARR,@newarr);
    close($fh);
  } 
  if ($element eq "--mysql") {
    $mysql_index++;
    %struct = ();
    push(@mysql,%struct);
  }
  elsif ($element eq "--mysql-host") {
    $mysql[$mysql_index]{"host"} = shift(@ARR); 
  }
  elsif ($element eq "--mysql-port") {
    $mysql[$mysql_index]{"port"} = shift(@ARR);
  }
  elsif ($element eq "--mysql-user") {
    $mysql[$mysql_index]{"user"} = shift(@ARR);
  }
  elsif ($element eq "--mysql-password") {
    $mysql[$mysql_index]{"password"} = shift(@ARR);
  }
  elsif ($element eq "--snapshot") {
    $snapshot_index++;
    %struct = ();
    push(@snapshots,%struct);
  }
  elsif ($element eq "--snapshot-name") {
    $snapshots[$snapshot_index]{"name"} = shift(@ARR);
  }
  elsif ($element eq "--snapshot-filesystem") {
    $snapshots[$snapshot_index]{"filesystem"} = shift(@ARR);
  }
  elsif ($element eq "--ssh-service") {
    $ssh_services_index++;
    %struct = ();
    push(@ssh_services,%struct);
  }
  elsif ($element eq "--ssh-service-host") {
    $ssh_services[$ssh_services_index]{"host"} = shift(@ARR);
  }
  elsif ($element eq "--ssh-service-name") {
    $ssh_services[$ssh_services_index]{"service"} = shift(@ARR);
  }
  elsif ($element eq "--ssh-command") {
    $ssh_commands_index++;
    %struct = ();
    push(@ssh_commands,%struct);
  }
  elsif ($element eq "--ssh-command-host") {
    $ssh_commands[$ssh_commands_index]{"host"} = shift(@ARR);
  }
  elsif ($element eq "--ssh-command-start") {
    $ssh_commands[$ssh_commands_index]{"start"} = shift(@ARR);
  }
  elsif ($element eq "--ssh-command-stop") {
    $ssh_commands[$ssh_commands_index]{"stop"} = shift(@ARR);
  }
}

if($command eq "pre") {
  # Connect to the local database
  foreach $d (@mysql) {
    my $dbh = DBI->connect("DBI:mysql:mysql:$d->{'host'}:$d->{'port'}, $d->{'user'}, $d->{'password'}") || die "Can't connect to mysql";
    $dbh->do('FLUSH TABLES WITH READ LOCK;') || die "Can't flush and lock mysql tables";
    $d->{"dbh"} = $dbh
  }

  foreach $s (@ssh_services) {
    system ("ssh -q -x  $s->{'host'} service $s->{'service'} stop") && die "Could not stop service";
  }

  foreach $s (@ssh_commands) {
    system ("ssh -q -x  $s->{'host'}  $s->{'stop'}") && die "Could not run ssh command";
  }

  foreach $s (@snapshots) {
    mkdir "/snapshots/$s->{'name'}";
    # Create LVM snapshot volume
    system("/sbin/lvcreate -L20G -s -n $s->{'name'} $s->{'filesystem'}") && die "Could not create snapshot";

    #mount snapshot
    system("mount /dev/vg/$s->{'name'} /snapshots/$s->{'name'}") && die "Could not mount snapshot";
  }

  foreach $d (@mysql) {
    my $dbh = $d{"dbh"};
    $dbh->do('UNLOCK TABLES;') || die;
    $dbh->disconnect();
  }

  foreach $s (@ssh_services) {
    system ("ssh -q -x  $s->{'host'}  service $s->{'service'} start") && die "Could not start service";
  }

  foreach $s (@ssh_commands) {
    system ("ssh -q -x  $s->{'host'}  $s->{'start'}") && die "Could not run ssh post-command";
  }

  exit 0;
}
elsif($command eq "post") {
  foreach $s (@snapshots) {
    system("umount /snapshots/$s->{'name'}") && die "Could not umount snapshot";

    system("/sbin/lvremove -f /dev/vg/$s->{'name'}") && die "Could not remove snapshot";
  }

  exit 0;
} 

else {
  die "No command specified. pre or post expected";
}
